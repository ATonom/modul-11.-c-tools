﻿#include <iostream>

// Вызывает функцию std::cout
void print()
{
    // Вызываем оператор << на объекте типа std::cout
    std::cout << "Hello Skillbox!" << std::endl;
};

/*
 * Присваеваем значения переменным.
 * Печатаем Hello Skillbox! и значение
 * переменной random.
*/
int main()
{
    int x = 100;
    int y = x + 100;

    int b = 0;
    b = b + 2;

    int test;
    test = 100500;

    int test2 = 1005001;

    int mult = x * y;

    int random = 1002;

    print();
    std::cout << random << std::endl;
}